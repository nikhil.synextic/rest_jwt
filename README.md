# CodeIgniter Rest Server With JWT Authentication

A fully RESTful server implementation for CodeIgniter using JWT for Authentication
## Notes

- Import Database from /db/rest_jwt.db

- Test it with postman/insomnia

- Create post method from postman for user registration "http://localhost/ci-rest-jwt/api/auth/register"
- Add this to body multipart form :

	useremail = nikhil

	useremail = nikhil@gmail.com
	
	password = 12345

	confirm_password = 12345

- If your registration success you will get email to verify email
	note- I am not providing any email encrption (in feature we can use that)

- Verify email 	post method from postman to verify useremail "http://localhost/ci-rest-jwt/api/auth/verifyuser"
- Add this to body multipart form :

	useremail = nikhil@gmail.com
- With this api user will activated and can login now. 

- Create post method from postman for user authentication "http://localhost/ci-rest-jwt/api/auth/login"
- Add this to body multipart form :
	
	useremail = nikhil@gmail.com
	
	password = 12345

- If your authentication success you will get generated token response
- To test it, go Create post method from postman "http://localhost/ci-rest-jwt/api/main/test" and then you can attach that generated token you've got to the header authentication bearer token. see example bellow :

	Authentication: Bearer "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjIiLCJ1c2VybmFtZSI6ImRvZGk"

