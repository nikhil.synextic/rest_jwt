<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Auth extends BD_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key

        $this->load->helper('form');
        $this->load->library('form_validation');
        // $this->load->library('session');
       
        $this->load->library('email');
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('M_main');
    }

    

    public function login_post()
    {
        $this->form_validation->set_rules('useremail','User email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password','Password', 'required');
        
        if($this->form_validation->run() == false){
            $data['error'] = 'Wrong inputs';
            $data['message'] = validation_errors();
            $this->set_response($data, REST_Controller::HTTP_NOT_FOUND); //This is the respon if failed
            
        }else{

        $u = $this->post('username'); //Username Posted
        $e = $this->post('useremail'); //Useremail Posted
        $p = sha1($this->post('password')); //Pasword Posted
        $q = array('useremail' => $e); //For where query condition
        $kunci = $this->config->item('thekey');
        $val = $this->M_main->get_user($q)->row(); //Model to get single data row from database base on useremail
        if($this->M_main->get_user($q)->num_rows() == 0){$this->response(['status' => 'Invalid Login'], REST_Controller::HTTP_NOT_FOUND);}
		$match = $val->password;   //Get password for user from database
        $status = $val->status;   //Get status for user from database
        if($status == 'Pending'){  //Condition if user not active
            $this->set_response(['status' => 'You have to activate account first.'], REST_Controller::HTTP_NOT_FOUND); //This is the respon if failed
        }else if($status == 'Inactive'){  //Condition if user blocked
            $this->set_response(['status' => 'You are blocked, contact to admin.'], REST_Controller::HTTP_NOT_FOUND); //This is the respon if failed
        }else if($p == $match){  //Condition if password matched
        	$token['id'] = $val->id;  //From here
            $token['username'] = $u;
            $token['useremail'] = $e;
            $date = new DateTime();
            $token['iat'] = $date->getTimestamp();
            $token['exp'] = $date->getTimestamp() + 60*60*5; //To here is to generate token
            $output['token'] = JWT::encode($token,$kunci ); //This is the output token
            $this->set_response($output, REST_Controller::HTTP_OK); //This is the respon if success
        }
        else {
            $this->set_response(['status' => 'You are password is incorrect.'], REST_Controller::HTTP_NOT_FOUND); //This is the respon if failed
        }
    }
    }

    public function register_post()
    {
        $this->form_validation->set_rules('useremail','Employee email', 'trim|required|valid_email|is_unique[m_user.useremail]');
        $this->form_validation->set_rules('username','Username', 'trim|required');
        $this->form_validation->set_rules('password','Password', 'required');
        $this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'trim|required|matches[password]');
        
        if($this->form_validation->run() == false){
            $data['error'] = 'Wrong inputs';
            $data['message'] = validation_errors();
            $this->set_response($data, REST_Controller::HTTP_NOT_FOUND); //This is the respon if failed
            
        }else{
            //call db
            $data = array(
                'useremail' => $this->input->post('useremail'),
                'username' => $this->input->post('username'),
                'password' => sha1($this->input->post('password'))
            );
            
            
            if($lastinsertid=$this->M_main->insertUser($data)){
            
                //send confirm mail
                if($this->M_main->sendEmail($this->input->post('useremail'))){
                    //$msg = "Successfully registered with the sysytem.Conformation link has been sent to: ".$this->input->post('useremail');
                    $this->set_response(['status' => 'Your registered successfully, Please verify your email.'], REST_Controller::HTTP_OK); //This is the respon if success
                }else{
                    //$error = "Error, Cannot send mail to  user , delete user";
                    $this->M_main->deleteUser($lastinsertid);
                    $this->set_response(['status' => 'Your registration failed, Failed to send email.'], REST_Controller::HTTP_NOT_FOUND); //This is the respon if failed
                }
                
            }else{
                //$error = "Error, Cannot insert new user details!";
                $this->set_response(['status' => 'Your registration is failed, please try again.'], REST_Controller::HTTP_NOT_FOUND); //This is the respon if failed
            }
        }
    }

    public function verifyuser_post()
    {
        $this->form_validation->set_rules('useremail','User email', 'trim|required|valid_email');
        
        if($this->form_validation->run() == false){
            $data['error'] = 'Wrong inputs';
            $data['message'] = validation_errors();
            $this->set_response($data, REST_Controller::HTTP_NOT_FOUND); //This is the respon if failed
            
        }else{
            $q = array('useremail' => $this->input->post('useremail')); //For where query condition
            $val = $this->M_main->get_user($q)->row(); //Model to get single data row from database base on useremail
            if($this->M_main->get_user($q)->num_rows() == 0){$this->response(['status' => 'Invalid Request, User is not present.'], REST_Controller::HTTP_NOT_FOUND);}
            $status = $val->status;   //Get status for user from database
            if($status == 'Pending'){  //Condition if user not active
                //call db
                $data = array('useremail' => $this->input->post('useremail'));
                if($this->M_main->verifyEmail($data)){
                    $this->set_response(['status' => 'Your account is activated, please login.'], REST_Controller::HTTP_OK); //This is the respon if success
                }else{
                    $this->set_response(['status' => 'Your account is not activate yet, please try again.'], REST_Controller::HTTP_NOT_FOUND); //This is the respon if failed
                }
            }else if($status == 'Inactive'){  //Condition if user blocked
                $this->set_response(['status' => 'You are blocked, contact to admin.'], REST_Controller::HTTP_NOT_FOUND); //This is the respon if failed
            }else if($status == 'Active'){  //Condition if user already active
                $this->set_response(['status' => 'You are already active, please login.'], REST_Controller::HTTP_OK); //This is the respon if success
            }
            
        }
    }

}
