<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_main extends CI_Model{

	function __construct(){
        
        parent::__construct();
        $this->load->database();
    }

    //get user details from m_user table
	function get_user($q) {
		return $this->db->get_where('m_user',$q);
	}

	//insert user details to m_user table
    public function insertUser($data){
        
        $this->db->insert('m_user',$data);
        return $this->db->insert_id();
      
    }

    public function deleteUser($data){
        $this->db->where('id', $data);
        $this->db->delete('m_user');
    }

     //send confirm mail
    public function sendEmail($receiver){
        $from = "nikdev45@gmail.com";    //senders email address
        $subject = 'Verify email address';  //email subject
        
        //sending confirmEmail($receiver) function calling link to the user, inside message body
        $message = 'Dear User,<br><br> Please click on the below activation link to verify your email address<br><br>
        <a href=\'http://localhost/rest_jwt/api/auth/verifyuser/'.$receiver.'\'>http://localhost/rest_jwt/api/auth/verifyuser/'. $receiver .'</a><br><br>Thanks';
        
        
        
        //config email settings
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.gmail.com';
        $config['smtp_port'] = '465';
        $config['smtp_user'] = $from;
        $config['smtp_pass'] = 'Developer@PHP';  //sender's password
        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = 'TRUE';
        $config['newline'] = "\r\n"; 
        
        $this->load->library('email', $config);
		$this->email->initialize($config);
        //send email
        $this->email->from($from);
        $this->email->to($receiver);
        $this->email->subject($subject);
        $this->email->message($message);

        if($this->email->send()){
            return true;
        }else{
            return false;
        }
        
       
    }

    //activate account
    function verifyEmail($email){
        $data = array('status' => 'Active');
        $this->db->where($email);
        return $this->db->update('m_user', $data);    //update status as 1 to make active user
    }

	
}